package org.avalith.fly.business;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import org.avalith.fly.data.AirlineRepository;
import org.avalith.fly.data.EntityAbstractRepository;
import org.avalith.fly.model.Airline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author wduck
 */
@Service
public class AirlineService extends EntityAbstractService<Airline>{

    private static final Logger LOGGER = Logger.getLogger(AirlineService.class.getName());        
    
    @Autowired
    AirlineRepository repository;
    
}
