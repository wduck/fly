package org.avalith.fly.business;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import org.avalith.fly.data.EntityAbstractRepository;
import org.avalith.fly.model.EntityAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author wduck
 * @param <T>
 */
@Service
public abstract class EntityAbstractService<T extends EntityAbstract> {

    private static final Logger LOGGER = Logger.getLogger(EntityAbstractService.class.getName());
    
    private T type;        
    
    @Autowired
    protected EntityAbstractRepository<T> repository;

    public EntityAbstractRepository<T> getRepository() {
        return repository;
    }

    public Page<T> findByCriteria(String criteria,  int pageNumber, int pageSize, String sortField){        
        Pageable pageableLocal = (sortField != null && !sortField.isEmpty()) ? 
                PageRequest.of(pageNumber, pageSize, Sort.by(sortField)) : 
                PageRequest.of(pageNumber, pageSize); 
         
        Page<T> resultsPage = repository.findAll(pageableLocal);        
        return resultsPage;
    }
    
    public T findById(Long id){
        return repository.findById(id).orElseThrow(() -> {
            //LOGGER.log(Level.FINE, String.format("%s with id [%s] not found", type.getClass().getSimpleName(), id));            
            System.err.println(String.format("%s with id [%s] not found", type.getClass().getSimpleName(), id));
            return new EntityNotFoundException(String.format("%s with id [%s] not found", type.getClass().getSimpleName(), id));
        });
    }
        
    public T save(T entity){
        return repository.save(entity);
    }
    
    
}
