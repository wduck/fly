/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.fly.data;

import org.avalith.fly.model.EntityAbstract;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author wduck
 * @param <T>
 */
public interface EntityAbstractRepository<T extends EntityAbstract> extends PagingAndSortingRepository<T, Long>{
    
}
