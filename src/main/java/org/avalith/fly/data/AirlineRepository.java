/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.fly.data;

import org.avalith.fly.model.Airline;

/**
 *
 * @author wduck
 */
public interface AirlineRepository extends EntityAbstractRepository<Airline>{
    
}
