package org.avalith.fly.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

@Entity
public class Route extends EntityAbstract{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@NotEmpty
	private String source;
	
	@NotEmpty
	private String target;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(source, target);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Route)) {
			return false;
		}
		Route other = (Route) obj;
		return Objects.equals(source, other.source) && Objects.equals(target, other.target);
	}

	@Override
	public String toString() {
		return "Route {source=" + source + ", target=" + target + ", id=" + getId() + "}";
	}
	
	
	
	
	
}
