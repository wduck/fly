/**
 * 
 */
package org.avalith.fly.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author wduck
 *
 */
@Entity
public class Airline extends EntityAbstract{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotEmpty
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "airline")	
	private Set<Airplane> aireplanes;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "airline")
	@OrderBy
	private Set<Flight> flights;

	public Airline() {
		super();
		aireplanes = new HashSet<Airplane>();
	}	
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void add(@NotNull Airplane aireplane) {		
		if (!aireplanes.contains(aireplane)) {
			aireplanes.add(aireplane);
			aireplane.setAirline(this);
		}		
	}
	
	public void remove(@NotNull Airplane aireplane) {
		boolean removed = aireplanes.remove(aireplane);
		if (removed) aireplane.setAirline(null);
	}
	
	public void add(@NotNull Flight object) {		
		if (!flights.contains(object)) {
			flights.add(object);
			object.setAirline(this);
		}		
	}
	
	public void remove(@NotNull Flight object) {
		boolean removed = flights.remove(object);
		if (removed) object.setAirline(null);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(name);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Airline))
			return false;
		Airline other = (Airline) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Airline {name=" + name + ", id=" + getId() + "}";
	}

}
