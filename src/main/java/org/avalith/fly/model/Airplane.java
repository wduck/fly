package org.avalith.fly.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Entity
public class Airplane extends EntityAbstract{
	
	private static final long serialVersionUID = 1L;

	@NotEmpty
	private String enrollmentNumber;
	
	private Integer numberPassengers = 0;
	
	// tripulantes
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "airplane")	
	private Set<Person> crews;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn
	private Airline airline;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "airplane")
	@OrderBy
	private Set<Flight> flights;
	
	public Airplane() {
		super();
		numberPassengers = 0;
		crews = new HashSet<Person>();
		flights = new HashSet<Flight>();
	}	
	
	public void add(@NotNull Person crew) {		
		if (!crews.contains(crew)) {
			crews.add(crew);
			crew.setAirplane(this);
		}		
	}
	
	public void remove(@NotNull Person crew) {
		boolean removed = crews.remove(crew);
		if (removed) crew.setAirplane(null);
	}
	
	public void add(@NotNull Flight object) {		
		if (!flights.contains(object)) {
			flights.add(object);
			object.setAirplane(this);
		}		
	}
	
	public void remove(@NotNull Flight object) {
		boolean removed = flights.remove(object);
		if (removed) object.setAirplane(null);
	}

	public String getEnrollmentNumber() {
		return enrollmentNumber;
	}

	public void setEnrollmentNumber(String enrollmentNumber) {
		this.enrollmentNumber = enrollmentNumber;
	}

	public Integer getNumberPassengers() {
		return numberPassengers;
	}
	
	public void setNumberPassengers(Integer numberPassengers) {
		this.numberPassengers = numberPassengers;
	}

	public Set<Person> getCrews() {
		return crews;
	}

	public void setCrews(Set<Person> crews) {
		this.crews = crews;
	}	

	public Airline getAirline() {
		return airline;
	}

	public void setAirline(Airline airline) {
		this.airline = airline;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(enrollmentNumber, numberPassengers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Airplane)) {
			return false;
		}
		Airplane other = (Airplane) obj;
		return Objects.equals(enrollmentNumber, other.enrollmentNumber)
				&& Objects.equals(numberPassengers, other.numberPassengers);
	}

	@Override
	public String toString() {
		return "Airplane {enrollmentNumber=" + enrollmentNumber + ", numberPassengers=" + numberPassengers
				+ ", id()=" + getId() + "}";
	}
	
	
	
	
	
}
