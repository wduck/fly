/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.fly.boundary;

import java.net.URI;
import javax.validation.Valid;
import org.avalith.fly.business.PersonService;
import org.avalith.fly.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author wduck
 */
@RestController
@RequestMapping("/persons")
//@Api(value = "/persons")
public class PersonController {
    
    @Autowired
    PersonService personService;
    
    @GetMapping
    public Page<Person> findByCriteria(
            @RequestParam(name = "criteria", defaultValue = "") String search,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam(defaultValue = "") String sort) {

        return personService.findByCriteria(search, pageNumber, pageSize, sort);
    }
    
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> create(@Valid @RequestBody Person request) {                
        Person saved = personService.save(request);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(saved.getId()).toUri();        
        return ResponseEntity.created(location).body(saved);
    }    
    
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> findById(@PathVariable(value = "id") @Valid Long id) {
        System.out.println("-----> " + personService.findById(id));
        return new ResponseEntity<>(personService.findById(id), HttpStatus.OK);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> update(@PathVariable(value = "id") Long id,
                                         @Valid @RequestBody Person request) {
        request.setId(id);        
        Person saved = personService.save(request);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }
    
}
