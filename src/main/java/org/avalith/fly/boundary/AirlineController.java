/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.fly.boundary;

import java.net.URI;
import javax.validation.Valid;
import org.avalith.fly.business.AirlineService;
import org.avalith.fly.model.Airline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author wduck
 */
@RestController
@RequestMapping("/airlines")
//@Api(value = "/airlines")
public class AirlineController {
    
    @Autowired
    AirlineService airlineService;
    
    @GetMapping
    public Page<Airline> findByCriteria(
            @RequestParam(name = "criteria", defaultValue = "") String search,
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam(defaultValue = "") String sort) {

        return airlineService.findByCriteria(search, pageNumber, pageSize, sort);
    }
    
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Airline> create(@Valid @RequestBody Airline request) {                
        Airline saved = airlineService.save(request);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(saved.getId()).toUri();        
        return ResponseEntity.created(location).body(saved);
    }    
    
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Airline> findById(@PathVariable(value = "id") @Valid Long id) {
        System.out.println("-----> " + airlineService.findById(id));
        return new ResponseEntity<>(airlineService.findById(id), HttpStatus.OK);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Airline> update(@PathVariable(value = "id") Long id,
                                         @Valid @RequestBody Airline request) {
        request.setId(id);        
        Airline saved = airlineService.save(request);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }
    
}
