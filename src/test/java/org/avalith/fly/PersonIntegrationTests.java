package org.avalith.fly;

import java.net.URI;
import java.net.URISyntaxException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import org.avalith.fly.model.Person;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@SpringBootTest(classes = FlyApplication.class,
        webEnvironment = WebEnvironment.RANDOM_PORT)
class PersonIntegrationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    HttpHeaders headers = new HttpHeaders();
    
    private static String RESOURCE_MAIN = "/persons";
    
    
    @Test
    void contextLoads() {    
    }
    
    

    @Test
    void createPerson() {
        Person person = buildPerson();
        ResponseEntity<String> responseEntity = this.restTemplate
			.postForEntity("http://localhost:" + port + RESOURCE_MAIN, person, String.class);
	assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    void createPersonWithValidation() {
        Person person = buildPerson();
        person.setDni(null);
        ResponseEntity<String> responseEntity = this.restTemplate
			.postForEntity("http://localhost:" + port + RESOURCE_MAIN, person, String.class);
	assertEquals(500, responseEntity.getStatusCodeValue());
        System.out.println("=== Entity Error Message: " + responseEntity.getBody());
    }
    
    @Test
    void updatePerson() throws URISyntaxException {
        Person person = buildPerson();
        person.setEmail("arch@gmail.com");
        HttpEntity<Person> entity = new HttpEntity<Person>(person, headers);
        
        ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/1"),
				HttpMethod.PUT, entity, String.class);        
	assertEquals(200, response.getStatusCodeValue());
        System.out.println("=== Entity updated person: " + response.getBody());
    }
    
    @Test
    void retrievePerson() {        
        ResponseEntity<Person> responseEntity = this.restTemplate
			.getForEntity("http://localhost:" + port + RESOURCE_MAIN + "/1", Person.class);        
        System.out.println("=== OBJECT 1: " + responseEntity.getBody());
	assertEquals(200, responseEntity.getStatusCodeValue());
        System.out.println("=== Entity Retrieve Person: " + responseEntity.getBody());
    }
    
    @Test
    void retrievePersons() {        
        ResponseEntity<String> responseEntity = this.restTemplate
			.getForEntity("http://localhost:" + port + RESOURCE_MAIN, String.class);        
	assertEquals(200, responseEntity.getStatusCodeValue());        
        System.out.println("=== Entity Retrieves Person: " + responseEntity.getBody());
    }
    
    
    private Person buildPerson(){
        Person p = new Person();
        p.setDni("1103498092");
        p.setFirstName("ANALIA ROMINA");
        p.setLastName("VALAREZO B");
        p.setEmail("ar@gmail.com");
        return p;
    }
    
    URI createURLWithPort(String path) throws URISyntaxException{
        StringBuilder urlBase = new StringBuilder("http://localhost:" + port);
        urlBase = urlBase.append(RESOURCE_MAIN);
        if (path != null && !path.isEmpty()){
            urlBase = urlBase.append(path);
        }
        return new URI(urlBase.toString());
    }
    
    URI createURLWithPort() throws URISyntaxException{        
        return createURLWithPort(null);
    }

}
