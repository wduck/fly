package org.avalith.fly;

import java.net.URI;
import java.net.URISyntaxException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import org.avalith.fly.model.Airline;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@SpringBootTest(classes = FlyApplication.class,
        webEnvironment = WebEnvironment.RANDOM_PORT)
class AirlineIntegrationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    HttpHeaders headers = new HttpHeaders();
    
    private static String RESOURCE_MAIN = "/airlines";
    
    @Test
    void contextLoads() {    
    }

    @Test
    void createAirline() {
        Airline airline = buildAirline();
        ResponseEntity<String> responseEntity = this.restTemplate
			.postForEntity("http://localhost:" + port + RESOURCE_MAIN, airline, String.class);
	assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    void createAirlineWithValidation() {
        Airline object = buildAirline();
        object.setName(null);
        ResponseEntity<String> responseEntity = this.restTemplate
			.postForEntity("http://localhost:" + port + RESOURCE_MAIN, object, String.class);
	assertEquals(500, responseEntity.getStatusCodeValue());
        System.out.println("=== Entity Error Message: " + responseEntity.getBody());
    }
    
    @Test
    void updateAirline() throws URISyntaxException {
        Airline object = buildAirline();
        object.setName("IBERO AMERICA");
        HttpEntity<Airline> entity = new HttpEntity<Airline>(object, headers);
        
        ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/1"),
				HttpMethod.PUT, entity, String.class);        
	assertEquals(200, response.getStatusCodeValue());
        System.out.println("=== Entity updated Airline: " + response.getBody());
    }
    
    @Test
    void retrieveAirline() {        
        ResponseEntity<Airline> responseEntity = this.restTemplate
			.getForEntity("http://localhost:" + port + RESOURCE_MAIN + "/1", Airline.class);        
        System.out.println("=== OBJECT 1: " + responseEntity.getBody());
	assertEquals(200, responseEntity.getStatusCodeValue());
        System.out.println("=== Entity Retrieve Airline: " + responseEntity.getBody());
    }
    
    @Test
    void retrieveAirlines() {        
        ResponseEntity<String> responseEntity = this.restTemplate
			.getForEntity("http://localhost:" + port + RESOURCE_MAIN, String.class);        
	assertEquals(200, responseEntity.getStatusCodeValue());        
        System.out.println("=== Entity Retrieves Airline: " + responseEntity.getBody());
    }
    
    
    private Airline buildAirline(){
        Airline a = new Airline();
        a.setName("IBERO");
        return a;
    }
    
    URI createURLWithPort(String path) throws URISyntaxException{
        StringBuilder urlBase = new StringBuilder("http://localhost:" + port);
        urlBase = urlBase.append(RESOURCE_MAIN);
        if (path != null && !path.isEmpty()){
            urlBase = urlBase.append(path);
        }
        return new URI(urlBase.toString());
    }
    
    URI createURLWithPort() throws URISyntaxException{        
        return createURLWithPort(null);
    }

}
